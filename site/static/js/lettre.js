// FUNCTIONS ------------------------------------------------------------
overflow = function(element) {
    if (element.scrollHeight > element.clientHeight) {
        return element.scrollHeight - element.clientHeight
    } else {
        return false
    }
}

randomNumber = function(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

imprimer = function() {
    console.log("Ça part à l'impression");
    window.print();
}

startAnimation = function() {
    document.getElementById("page").style.color = "black";
    INSTRU.play();

    const lol = document.getElementById("ceparti");
    lol.style.opacity = "0";

    const delay = 40;
    setTimeout(() => { lol.style.display = "none" }, delay);

    let j = 1;
    for (let el of textes) {
        let contenu = el.innerText;
        el.innerHTML = " ";
        for (let i = 0; i < contenu.length; i++) {
            // const delay = randomNumber(-10, 20);
            const offset = 0;
            setTimeout(() => { el.innerHTML += contenu.charAt(i) }, (delay * j) + offset);

            j++;
        }
    }

    const imprimer = document.getElementById("imprimer");
    const signature = document.getElementById("signature");

    // Pause de l'INSTRU
    setTimeout(() => { INSTRU.pause() }, delay * j);

    setTimeout(function() {
        // signature.style.transform = "scaleX(1)";
        signature.style.opacity = "1";

        imprimer.style.display = "block";
        setTimeout(() => imprimer.style.opacity = "1", delay);
    }, (delay + 1) * j);

}

// DECLARATIONS ------------------------------------------------------------
let today = new Date();
const dd = String(today.getDate()).padStart(2, "0");
const mm = String(today.getMonth() + 1).padStart(2, "0");
const yyyy = today.getFullYear();
today = `${dd}/${mm}/${yyyy}`;

const ville = document.getElementById("lieu");
ville.innerHTML = `${ville.innerHTML}, le ${today}`;

const textes = document.getElementsByClassName("type");

// Forcer la largeur des éléments alignés à droite pour avoir l'air écrit plus naturellement
// const right = document.getElementsByClassName("droite");
// for (let el of right) {
//     el.style.width = `${el.getBoundingClientRect().width}px`;
// }

// Recalculer la taille des parties en px pour l'utiliser dans les calculs de translate3d()
const rayon = document.getElementById("page").getBoundingClientRect().height / 6;
document.getElementById('page').style.setProperty('--radius', `${rayon}px`);

const INSTRU = document.getElementById("typing-sound");

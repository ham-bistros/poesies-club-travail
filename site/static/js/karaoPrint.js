/* ---- MATH ---- */

function map_range(value, low1, high1, low2, high2) {

	return low2 + (high2 - low2) * (value - low1) / (high1 - low1);

}

/* ---- STRINGS ---- */

function getVidId(src){

	var last = src.lastIndexOf("v=")+2;
	var vidId = src.substr(last);

	return vidId;

}

/* ---- DOM ---- */

function setIds(){

	tracks.forEach((track, index) => {

		var id = "track-"+index;
		track.id=id;

		tracksIds.push(id);
		counts.push(0);

		track.dataset.num=index;

	});
}


/* ---- KARAOKE ---- */

function counter(track, index){

	var frameRate = 10;

	var inter = (track, index) => {

		var time =  Math.trunc(track.getCurrentTime()*10)/10;
		var nowFrame = karaokes[index].querySelector("[data-slide='"+time+"']");

		setProgressBar(track, time, index);

		if(nowFrame){

			if(!nowFrame.classList.contains("sing")){

				var slides = karaokes[index].querySelectorAll(".slides>div");

				slides.forEach((slide) => {

					if(slide.classList.contains("sing")){

						slide.classList.remove("sing");
					}

				});

				nowFrame.classList.add("sing");

			}

		}

	}

	return setInterval(inter, frameRate, track, index);

}

function setKaraoke(player, event, index){

	var state = event.data;

	if(state == 1){

		counts[index] = counter(player, index);

	}

	if(state == 2){

		clearInterval(counts[index]);
	}

	if(state == 0){

		clearInterval(counts[index]);
		count = 0;

	}
}


function setCurrentFrame(index, seconds){

	var slides = karaokes[index].querySelectorAll(".slides>div");
	var slideTimes = []; 


	slides.forEach((slide) => {

		var slideTime = parseFloat(slide.dataset.slide);

		if(slideTime <= seconds){

			slideTimes.push(slideTime);

		}

		if(slide.classList.contains("sing")){

			slide.classList.remove("sing");

		}

	});

	var thatSlide = karaokes[index].querySelector("[data-slide='"+slideTimes[slideTimes.length-1]+"']");

	thatSlide.classList.add("sing");

}

function fillTotalTimes(player){

	totalTimes.push(player.getDuration());
}


/* CONTROLS PLAYER */

function setProgressBar(player, currentTime, index){

	var bar = progressBars[index];
	var totalTime = totalTimes[index];
	var progress = bar.querySelector(".progress");
	var length = bar.offsetWidth;

	var progLength = Math.round(map_range(currentTime, 0, totalTime, 0, length));

	progress.style.width=progLength+"px";

	setTimer(index, currentTime);

}

function setTimer(index, time){

	var counter = karaokes[index].querySelector(".seconds");

	counter.innerHTML=time+" (s)";
}

function navigateInProgressBar(player, index){

	let bar = progressBars[index];

	bar.addEventListener("click", (e) => {

		var rect = e.target.getBoundingClientRect();
		var pos = e.clientX - rect.left;
		var length = bar.offsetWidth;
		var progress = bar.querySelector(".progress");

		var secs = Math.round(map_range(pos, 0, length, 0, totalTimes[index]));

		player.seekTo(secs);
		progress.style.width=pos+"px";

		if(player.getPlayerState() == 1){

			player.playVideo();

		}



		setCurrentFrame(index, secs);
		setTimer(index, secs);
	});
}

function setPlayPauseButton(player, index){

	var playButt = karaokes[index].querySelector("[data-action='playpause']");

	playButt.addEventListener("click", () => {

		if(playButt.classList.contains("pause")){

			player.playVideo();
			playButt.classList.remove("pause");
			playButt.classList.add("play");

		}else{

			player.pauseVideo();
			playButt.classList.remove("play");
			playButt.classList.add("pause");

		}
	});
}


/*  ---- YOUTUBE EMBED  ---- */


function onYouTubeIframeAPIReady(){

	tracks.forEach((track, index) => {

		players[index] = new YT.Player(track.id, {

			videoId: getVidId(track.dataset.src),
			playerVars: { 'autoplay': 0, 'controls': 0,'autohide':1,'wmode':'opaque','origin':'http://localhost:80' },
			events: {

				'onReady': onPlayerReady,
				'onStateChange': onPlayerStateChange
			}

		});

	});
}


function onPlayerReady(event) {

	var index = parseInt(event.target.getIframe().dataset.num);

	fillTotalTimes(event.target);
	setPlayPauseButton(event.target, index);
	navigateInProgressBar(event.target, index);


}

function onPlayerStateChange(event){

	var index = parseInt(event.target.getIframe().dataset.num);

	setKaraoke(event.target, event, index);

}



/*  ---- LOAD ---- */


var karaokes = document.querySelectorAll(".karaoke");
var tracks = document.querySelectorAll(".track");
var tracksIds = [];
var progressBars = document.querySelectorAll(".bar");
var players = [];
var counts = [];
var totalTimes = [];

// LOAD YT API

var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

document.addEventListener("DOMContentLoaded", () => {

	setIds();

});


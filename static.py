#!/usr/bin/python

from jinja2 import Environment, FileSystemLoader
import random


class StaticSite:
    def __init__(
        self,
        variables,
        base_tpl="main",
        site_dir="site",
        templates_dir="templates",
    ):
        self.site_dir = site_dir
        self.templates_dir = templates_dir
        self.variables = variables
        self.base_tpl = base_tpl

    def templateToSite(self):
        env = Environment(loader=FileSystemLoader(self.templates_dir))
        template = env.get_template(f"{self.base_tpl}.jinja.html")

        output = template.render(self.variables)
        with open(self.site_dir + f"/{self.base_tpl}.html", "w") as fh:
            # print(output)
            fh.write(output)

    def build(self):
        self.templateToSite()


def content_from(file):
    with open(file, "r", encoding="utf-8") as f:
        return f.read().splitlines()


variables = {}

site = {
    "berceuse": {},
    "lettre": {},
    "karaoke": {},
}

pseudos = [
    "micheldu36",
    "TeckelToutDoux",
    "caro",
    "xXd4rk-j43n-p4ulXx",
    "prout",
    "audrey1180",
]

print("building page index.html…")
StaticSite({"titre": "index"}, "index").build()

for page in site:
    print(f"building {page}.html…")

    site[page]["titre"] = page
    # site[page]["nom"] = random.choice(pseudos)
    site[page]["nom"] = '"%s"' % random.choice(pseudos)

    content = content_from(f"txts/{page}.txt")

    if page == "karaoke":
        # Karaoké --------------------------
        site[page]["titre_chanson"] = '"%s"' % content[0]
        site[page]["bruit"] = '"%s"' % content[1]
        site[page]["lieu"] = '"%s"' % content[2]
        site[page]["phrase_longue"] = '"%s"' % content[3]
        site[page]["refrain"] = '"%s"' % content[4]
        site[page]["conclusion"] = '"%s"' % content[5]

    elif page == "lettre":
        # Lettre --------------------------
        site[page]["titre_chanson"] = '"%s"' % content[0]
        site[page]["nom_patron"] = '"%s"' % content[1]
        site[page]["lieu"] = '"%s"' % content[2]
        site[page]["propositions"] = content[3:]

    else:
        # Berceuse --------------------------
        site[page]["titre_chanson"] = '"%s"' % content[0]
        site[page]["phrases_douces"] = content[1:3]
        site[page]["phrases_rimes"] = content[3:]

    StaticSite(site[page], page).build()

print("done !")
